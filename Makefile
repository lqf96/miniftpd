# Variables
APP_NAME = server
LIB_PATH = -Llibasync
CXXFLAGS = -Wall -std=c++11 -Ilibasync/include -iquote headers -O3
DEPS = main.o methods.o middlewares.o server.o session.o data_conn.o
STRIP = strip

PLATFORM=$(shell uname -s)
# macOS extra flags
ifeq ($(PLATFORM), Darwin)
CXXFLAGS := -I/usr/local/include -D_XOPEN_SOURCE $(CXXFLAGS)
LIB_PATH := -L/usr/local/lib $(LIB_PATH)
endif
# Generate full dependencies list
DEPS := $(addprefix src/,$(DEPS))

# Targets
miniftpd: libasync.a $(DEPS)
	$(CXX) -O3 -o $(APP_NAME) $(DEPS) $(LIB_PATH) -lboost_regex -lboost_system -lboost_filesystem -lasync
	$(STRIP) $(APP_NAME)
libasync.a:
	make -C libasync static

# Other targets
clean:
	rm -f $(DEPS) $(APP_NAME)
