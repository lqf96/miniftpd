#include <arpa/inet.h>
#include <functional>
#include <string>
#include "server.h"
#include "session.h"

using libasync::Socket;
using std::string;

namespace miniftpd
{   //Server constructor
    Server::Server(ptree config) : data(std::make_shared<Server::ServerData>(config)) {}

    //Listen on given address
    void Server::listen(in_addr_t ip, in_port_t port)
    {   auto& config = this->data->config;
        //Not in idle state; do nothing
        if (this->status()!=ServerSocket::Status::IDLE)
            return;

        auto root = config.get<string>("server.root");
        //Create a session object for each incoming object
        this->server_sock.on("connect", [=](Socket sock)
        {   Session s(sock, root);
        });

        //Use default address
        if ((ip==INADDR_NONE)||(port==0))
        {   ip = config.get<in_addr_t>("server.ip");
            port = config.get<in_port_t>("server.port");
        }
        //Listen on given address
        this->server_sock.listen(htonl(ip), htons(port));
    }

    //Close server
    void Server::close()
    {   this->server_sock.close();
    }

    //Get server status
    ServerSocket::Status Server::status()
    {   return this->server_sock.status();
    }
}
