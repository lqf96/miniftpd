#include <arpa/inet.h>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include "session.h"
#include "data_conn.h"

using std::stoi;
using std::ostringstream;
using std::ifstream;
using std::ofstream;
using boost::regex;
using boost::smatch;
using boost::regex_search;
using boost::filesystem::is_directory;
using boost::filesystem::filesystem_error;
using boost::filesystem::create_directory;
using boost::filesystem::remove;
using boost::filesystem::is_regular_file;

namespace miniftpd
{   //FTP methods marco
    #undef FTP_METHOD
    #define FTP_METHOD(name) {#name, &Session::name},

    //Method handlers
    std::unordered_map<string, Session::MethodHandler> Session::method_handlers = {
        #include "data/ftp_methods"
    };

    //Subprocess read buffer
    const size_t SP_READ_BUF = 1024;

    //USER command
    string Session::USER(string req)
    {   auto data = this->data;
        auto req_pair = parse_req(req);

        //Set username and change log-in status
        data->username = req_pair.second;
        data->login_status = SessionData::USERNAME_GIVEN;
        //Password required
        return make_resp(331, RESP_DESC_331);
    }

    //PASS command
    string Session::PASS(string req)
    {   //TODO: User authentication mechanism
        data->login_status = SessionData::LOGGED;

        return make_resp(230, RESP_DESC_230);
    }

    //RETR command
    string Session::RETR(string req)
    {   auto req_pair = parse_req(req);
        auto data = this->data;

        path file_path(req_pair.second);
        if (!file_path.is_absolute())
            file_path = data->cwd/file_path;
        file_path = data->root/file_path;
        //Open given file
        ifstream fs(file_path.c_str());
        if (!fs)
            return make_resp(450, RESP_DESC_450);
        //Read file content into string
        string content, tmp;
        while (true)
        {   //Read a line and append to content
            getline(fs, tmp);
            content += tmp;
            //EOF break
            if (fs.eof())
                break;
            //Write newline
            content += '\n';
        }
        fs.close();

        //Write file content
        data->data_conn->send(content);
        //Return nothing
        return "";
    }

    //STOR command
    string Session::STOR(string req)
    {   auto req_pair = parse_req(req);
        auto data = this->data;

        path file_path(req_pair.second);
        if (!file_path.is_absolute())
            file_path = data->cwd/file_path;
        file_path = data->root/file_path;
        //Open given file
        auto fs = new ofstream(file_path.c_str());
        if (!(*fs))
            return make_resp(450, RESP_DESC_450);
        //Socket
        auto sock = this->sock;

        //Write file content
        data->data_conn->recv().then<void>([=](string data) mutable
        {   (*fs)<<data;
            //Close file and delete file stream
            fs->close();
            delete fs;
            //Send finished message
            sock.write(make_resp(226, RESP_DESC_226));
        });
        //Return nothing
        return "";
    }

    //QUIT command
    string Session::QUIT(string req)
    {   return make_resp(221, RESP_DESC_221);
    }

    //SYST command
    string Session::SYST(string req)
    {   return make_resp(215, RESP_DESC_215);
    }

    //TYPE command
    string Session::TYPE(string req)
    {   auto req_pair = parse_req(req);

        //Binary mode
        if (req_pair.second=="I")
            return make_resp(200, "Type set to I.");
        //Other modes are not supported
        else
            return make_resp(504, RESP_DESC_504);
    }

    //PORT command
    string Session::PORT(string req)
    {   auto data = this->data;
        regex tuple_rx("(\\d+),(\\d+),(\\d+),(\\d+),(\\d+),(\\d+)");
        smatch rx_match;

        //Tuple found; parse IP address and port
        if (regex_search(req, rx_match, tuple_rx))
        {   in_addr_t addr;

            //Convert to IP address
            uint8_t* _addr = (uint8_t*)(&addr);
            _addr[0] = stoi(rx_match[1]);
            _addr[1] = stoi(rx_match[2]);
            _addr[2] = stoi(rx_match[3]);
            _addr[3] = stoi(rx_match[4]);
            //Port
            in_port_t port = stoi(rx_match[5])*256+stoi(rx_match[6]);

            //Release previous data connection
            if (data->data_conn)
            {   delete data->data_conn;
                data->data_conn = nullptr;
            }
            data->conn_avail = true;
            //Create a new connection
            data->data_conn = new ActiveConnection(this->sock, addr, port);

            return make_resp(200, "PORT request received.");
        }
        //Tuple not found
        else
            return make_resp(501, RESP_DESC_501);
    }

    //PASV command
    string Session::PASV(string req)
    {   auto data = this->data;

        //Release previous data connection
        if (data->data_conn)
        {   delete data->data_conn;
            data->data_conn = nullptr;
        }
        data->conn_avail = true;
        //Get current socket address
        in_addr_t ip;
        in_port_t port;
        this->sock.local_addr(&ip, &port);
        //Create a new connection
        auto conn = new PassiveConnection(this->sock);
        data->data_conn = conn;
        //Get port
        port = ntohs(conn->port());

        uint8_t* _ip = (uint8_t*)(&ip);
        //Port data
        uint port1 = port/256,
            port2 = port%256;

        //Passive mode description
        ostringstream ss;
        ss<<"Entering passive mode. (" \
            <<((uint)(_ip[0]))<<',' \
            <<((uint)(_ip[1]))<<',' \
            <<((uint)(_ip[2]))<<',' \
            <<((uint)(_ip[3]))<<',' \
            <<port1<<','<<port2<<")";
        return make_resp(227, ss.str());
    }

    //CWD command
    string Session::CWD(string req)
    {   auto req_pair = parse_req(req);
        auto data = this->data;
        auto& cwd = data->cwd;
        ostringstream ss;

        path _path(req_pair.second);
        //Current working directory
        if (!_path.is_absolute())
            _path = (cwd/_path).normalize();
        //Check folder existance
        if (!is_directory(data->root/_path))
            return make_resp(450, RESP_DESC_450);
        //Update current working directory
        cwd = _path;

        ss<<"Working directory changed to \""<<cwd.string()<<"\".";
        //Response
        return make_resp(250, ss.str());
    }

    //CDUP command
    string Session::CDUP(string req)
    {   auto data = this->data;
        auto& cwd = data->cwd;
        ostringstream ss;

        //Current working directory
        cwd = (cwd/"..").normalize();
        ss<<"Working directory changed to \""<<cwd.string()<<"\".";
        //Response
        return make_resp(250, ss.str());
    }

    //DELE command
    string Session::DELE(string req)
    {   auto req_pair = parse_req(req);
        auto data = this->data;

        path rm_file_path(req_pair.second);
        //Non-absolute path
        if (!rm_file_path.is_absolute())
            rm_file_path = data->cwd/rm_file_path;
        try
        {   //Not exists or not regular file
            if (!is_regular_file(data->root/rm_file_path))
                return make_resp(450, RESP_DESC_450);
            //Remove file
            remove(data->root/rm_file_path);
        }
        catch (filesystem_error e)
        {   return make_resp(450, RESP_DESC_450);
        }

        return make_resp(250, "File removed successfully.");
    }

    //LIST command
    string Session::LIST(string req)
    {   auto data = this->data;

        //List path
        auto path = (data->root/data->cwd).string();
        auto cmd = "ls -l '"+path+"'";
        //Read buffer
        char read_buffer[SP_READ_BUF];
        string result;

        //Call "ls" and save its output to a string
        FILE* fp = popen(cmd.c_str(), "r");
        while (fgets(read_buffer, SP_READ_BUF, fp)!=NULL)
            result += read_buffer;
        pclose(fp);

        //Write list content
        data->data_conn->send(result);
        //Return nothing
        return "";
    }

    //MKD command
    string Session::MKD(string req)
    {   auto req_pair = parse_req(req);
        auto data = this->data;

        path new_dir_path(req_pair.second);
        //Non-absolute path
        if (!new_dir_path.is_absolute())
            new_dir_path = data->cwd/new_dir_path;
        //Create directory
        try
        {   create_directory(data->root/new_dir_path);
        }
        catch (filesystem_error e)
        {   return make_resp(450, RESP_DESC_450);
        }

        //Make response
        ostringstream ss;
        ss<<'\"'<<req_pair.second<<"\" created.";
        //Response
        return make_resp(257, ss.str());
    }

    //RMD command
    string Session::RMD(string req)
    {   auto req_pair = parse_req(req);
        auto data = this->data;

        path rm_dir_path(req_pair.second);
        //Non-absolute path
        if (!rm_dir_path.is_absolute())
            rm_dir_path = data->cwd/rm_dir_path;
        try
        {   //Not exists or not directory
            if (!is_directory(data->root/rm_dir_path))
                return make_resp(450, RESP_DESC_450);
            //Remove directory
            remove(data->root/rm_dir_path);
        }
        catch (filesystem_error e)
        {   return make_resp(450, RESP_DESC_450);
        }

        return make_resp(250, "Directory removed successfully.");
    }

    //PWD command
    string Session::PWD(string req)
    {   auto& cwd = this->data->cwd;
        ostringstream ss;

        ss<<'\"'<<cwd.string()<<'\"';
        //Response
        return make_resp(257, ss.str());
    }
}
