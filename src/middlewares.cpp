#include <libasync/taskloop.h>
#include "session.h"

namespace miniftpd
{   //FTP middleware marco
    #undef FTP_MIDDLEWARE
    #define FTP_MIDDLEWARE(name) &Session::name,

    //Middlewares
    std::vector<Session::MethodHandler> Session::middlewares = {
        #include "data/middlewares"
    };

    //Log-in checker
    string Session::login_checker(string req)
    {   auto data = this->data;
        auto req_pair = parse_req(req);

        //Access allowed
        bool allowed = (data->login_status==SessionData::LOGGED)
            ||((data->login_status==SessionData::NOT_LOGGED)&&(req_pair.first=="USER"))
            ||((data->login_status==SessionData::USERNAME_GIVEN)&&(req_pair.first=="PASS"));
        //Continue processing requests if allowed
        if (allowed)
            return this->next(req);
        //Not allowed
        else
            return make_resp(530, RESP_DESC_530);
    }

    //Quit helper
    string Session::quit_helper(string req)
    {   auto req_pair = parse_req(req);

        //TODO: If QUIT, close connection after sending goodbye message
        if (req_pair.first=="QUIT")
            ;
        //Continue processing
        return this->next(req);
    }
}
