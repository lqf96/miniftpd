#include <arpa/inet.h>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <libasync/taskloop.h>
#include <libasync/socket.h>
#include <libasync/promise.h>
#include "server.h"

using std::string;
using std::stoi;
using boost::property_tree::ptree;
using libasync::promise_init;
using libasync::reactor_init;
using libasync::TaskLoop;
using miniftpd::Server;

int main(int argc, char** argv)
{   //Libasync module initialization
    promise_init();
    reactor_init();

    //Configuration tree
    ptree config_tree;
    //Default configuration
    config_tree.put("server.ip", in_addr_t(INADDR_ANY));
    config_tree.put("server.port", in_port_t(21));
    config_tree.put("server.root", string("/tmp"));

    //Parse command line arguments
    int i = 1;
    while (i<argc)
    {   //Flag
        string flag = argv[i];
        i++;
        //Value
        string value;
        if (i<argc)
        {   value = argv[i];
            i++;
        }

        //Port
        if (flag=="-port")
        {   in_port_t port = stoi(value);
            config_tree.put("server.port", port);
        }
        //Root
        else if (flag=="-root")
            config_tree.put("server.root", value);
    }

    //Create server
    Server server(config_tree);
    //Listen on given address and port
    server.listen();

    //Start task loop
    TaskLoop::thread_loop().run();

    return 0;
}
