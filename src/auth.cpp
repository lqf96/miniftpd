#include <boost/none.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "auth.h"
#include "server.h"

using boost::property_tree::read_json;

namespace miniftpd
{   //Profile authentication backend constructor
    ProfileAuthBackend::ProfileAuthBackend(FTPServer server)
    {   //Get user profile directory path
        this->user_profile_dir = server.get_conf<string>("auth.profile.path");
    }

    //Check if the username can be handled by this backend
    bool ProfileAuthBackend::can_auth(string username)
    {   //Check existance of user profile
        auto user_profile_path = this->user_profile_dir/(username+".json");
        return boost::filesystem::exists(user_profile_path);
    }

    //Do authentication
    optional<ptree> auth(std::string username, std::string password)
    {   auto user_profile_path = this->user_profile_dir/(username+".json");
        ptree user_profile;

        //Load user profile
        read_json(user_profile_path.string(), user_profile);
        //Check password
        if (user_profile.get<string>("auth.password")!=password)
            return boost::none;

        return user_profile;
    }
}
