#include <stdlib.h>
#include <errno.h>
#include "data_conn.h"
#include "session.h"

using libasync::SocketError;

namespace miniftpd
{   //Ephemeral port range
    const static in_port_t EPH_PORT_BEGIN = 20000;
    const static in_port_t EPH_PORT_END = 60000;

    //Active connection constructor
    ActiveConnection::ActiveConnection(Socket _ctrl_sock, in_addr_t _addr, in_port_t _port)
        : ctrl_sock(_ctrl_sock), addr(_addr), port(_port)
    {   //Connection error
        this->sock.on("error", [=](SocketError e) mutable
        {   if (e.reason()==SocketError::Reason::CONNECT)
                this->ctrl_sock.write(make_resp(425, RESP_DESC_425));
        });
    }

    //Active connection destructor
    ActiveConnection::~ActiveConnection()
    {   //Close connection
        this->sock.close();
    }

    //Send data to client
    libasync::Promise<void> ActiveConnection::send(string data)
    {   return libasync::Promise<void>([=](libasync::PromiseCtx<void> ctx) mutable
        {   //Resolve promise when data sent
            this->sock.on("connect", [=]() mutable
            {   this->ctrl_sock.write(make_resp(150, RESP_DESC_150)).then<void>([=]() mutable
                {   return this->sock.write(data);
                }).then<void>([=]() mutable
                {   this->sock.close();
                    //Data transfer completed
                    return this->ctrl_sock.write(make_resp(226, RESP_DESC_226));
                }).then<void>([=]() mutable
                {   ctx.resolve();
                });
            });

            //Connect to given address and port
            this->sock.connect(addr, htons(port));
        });
    }

    //Receive data from client
    libasync::Promise<string> ActiveConnection::recv()
    {   //Get ready to read data from connection
        libasync::Promise<string> read_data_target([=](libasync::PromiseCtx<string> ctx) mutable
        {   //Received data
            auto recv_data = new string();

            //Data received
            this->sock.on("data", [=](string data) mutable
            {   //Resolve with data read
                *recv_data += data;
            });
            //Peer closed connection
            this->sock.on("end", [=]() mutable
            {   ctx.resolve(*recv_data);
                delete recv_data;
            });
        });

        return this->sock.connect(addr, htons(port)).then<void>([=]() mutable
        {   //Connection ready
            return this->ctrl_sock.write(make_resp(150, RESP_DESC_150));
        }).then<string>([=]() mutable
        {   //Read data from socket
            return read_data_target;
        });
    }

    //Passive connection constructor
    PassiveConnection::PassiveConnection(Socket _ctrl_sock) : ctrl_sock(_ctrl_sock), closed(false)
    {   //Connect event
        server_sock.on("connect", [=](Socket sock) mutable
        {   //Send client connected acknowledge information
            this->ctrl_sock.write(make_resp(150, RESP_DESC_150));

            this->sock = sock;
            //Data received
            sock.on("data", [=](string data) mutable
            {   this->recv_data += data;
            });
            //Peer closed connection
            sock.on("end", [=]() mutable
            {   this->closed = true;
            });
        });

        //Get control socket address
        in_addr_t addr;
        in_port_t port;
        this->ctrl_sock.local_addr(&addr, &port);

        //Try binding to a random port
        while (true)
        {   port = rand()%(EPH_PORT_END-EPH_PORT_BEGIN)+EPH_PORT_BEGIN;
            try
            {   this->server_sock.listen(addr, htons(port));
                break;
            }
            catch (SocketError e)
            {   //Retry if address in use; throw again for other error
                if ((e.reason()!=SocketError::Reason::BIND)||(e.error_num()!=EADDRINUSE))
                    throw e;
            }
        }
    }

    //Passive connection destructor
    PassiveConnection::~PassiveConnection()
    {   //Close socket
        if (this->sock)
            this->sock->close();
        //Close server socket
        this->server_sock.close();
    }

    //Get port number
    in_port_t PassiveConnection::port()
    {   in_addr_t addr;
        in_port_t port;
        this->server_sock.local_addr(&addr, &port);

        return port;
    }

    //Send data to client
    libasync::Promise<void> PassiveConnection::send(string data)
    {   auto action = [=](Socket sock) mutable
        {   return sock.write(data).then<void>([=]() mutable
            {   sock.close();
                //Data transfer completed
                return this->ctrl_sock.write(make_resp(226, RESP_DESC_226));
            });
        };

        //Socket conncted
        if (this->sock)
            return action(*this->sock);
        //Socket not connected
        else
            return libasync::Promise<void>([=](libasync::PromiseCtx<void> ctx) mutable
            {   this->server_sock.on("connect", [=](Socket sock) mutable
                {   action(sock).then<void>([=]() mutable
                    {   ctx.resolve();
                    });
                });
            });
    }

    //Receive data from client
    libasync::Promise<string> PassiveConnection::recv()
    {   //Connection closed
        if (this->closed)
            return libasync::Promise<string>::resolved(this->recv_data);
        //Socket connected
        else if (this->sock)
            return libasync::Promise<string>([=](libasync::PromiseCtx<string> ctx) mutable
            {   this->sock->on("end", [=]() mutable
                {   ctx.resolve(this->recv_data);
                });
            });
        //Socket not connected
        else
            return libasync::Promise<string>([=](libasync::PromiseCtx<string> ctx) mutable
            {   this->server_sock.on("connect", [=](Socket sock) mutable
                {   if (!this->sock)
                        this->sock = sock;
                    ctx.resolve(this->recv());
                });
            });
    }
}
