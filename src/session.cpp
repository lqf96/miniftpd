#include <functional>
#include <sstream>
#include <vector>
#include <utility>
#include <boost/algorithm/string.hpp>
#include "session.h"

using std::istringstream;
using std::ostringstream;
using std::getline;
using std::string;
using boost::split;
using boost::is_any_of;

namespace miniftpd
{   //FTP response code marco
    #undef FTP_RESPONSE
    #define FTP_RESPONSE(code, desc) const char* RESP_DESC_##code = desc;
    //FTP response code & description
    #include "data/responses"

    //Make response
    extern string make_resp(unsigned int resp_code, string description);

    //Session constructor
    Session::Session(Socket sock, string root) : sock(sock), data(std::make_shared<SessionData>(root))
    {   Session s = *this;

        //Listen to data event
        sock.on("data", [=](string data) mutable
        {   s.on_data(data);
        });
        //Peer closed connection
        sock.on("end", [=]() mutable
        {   s.on_close();
        });

        //Welcome message
        sock.write(make_resp(220, "Miniftpd server welcome."));
    }

    //Data handler
    void Session::on_data(string req)
    {   auto data = this->data;
        //Set current middleware index
        data->middleware_index = 0;

        this->sock.write(this->next(req));
    }

    //Close handler
    void Session::on_close()
    {   this->sock.close();
    }

    //Call next middleware
    string Session::next(string req)
    {   auto data = this->data;

        //No more middlewares
        if (data->middleware_index>=Session::middlewares.size())
        {   auto req_pair = parse_req(req);
            auto handler = Session::method_handlers[req_pair.first];

            //Known method
            if (handler)
                return (this->*handler)(req);
            //Unknown
            else
                return make_resp(502, RESP_DESC_502);
        }
        //Call current middleware
        else
        {   auto handler = Session::middlewares[data->middleware_index];
            data->middleware_index++;

            //Call middleware
            return (this->*handler)(req);
        }
    }

    //Parse request
    std::pair<string, string> parse_req(string req)
    {   istringstream ss(req);
        string method_name, description;

        ss>>method_name;
        //Skip white space character
        ss.get();
        getline(ss, description, '\r');

        return make_pair(method_name, description);
    }

    //Make response
    string make_resp(unsigned int resp_code, string description)
    {   ostringstream ss;
        std::vector<string> split_desc;

        //Split description string
        split(split_desc, description, is_any_of("\n"));
        //Generate response
        for (size_t i=0;i<split_desc.size()-1;i++)
            ss<<resp_code<<'-'<<split_desc[i]<<"\r\n";
        ss<<resp_code<<' '<<split_desc.back()<<"\r\n";

        return ss.str();
    }
}
