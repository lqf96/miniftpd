# Miniftpd
Miniftpd, a lightweight and asynchronous FTP server.

## Features
* Customize port and root directory upon launching server program.
* Simultaneously handle multiple clients without blocking.
* Support all basic FTP commands, including `USER`, `PASS`, `RETR`, `STOR`, `QUIT`, `SYST`, `TYPE`, `PORT` and `PASY`.
* Support more FTP commands, including `CWD`, `CDUP`, `DELE`, `LIST`, `MKD`, `RMD` and `PWD`.
* Flexible source code structure with method handler, request parser and middleware system.

## Usage
```bash
./server [-port PORT] [-root ROOT_DIR]
```
By default, the FTP server listens on port 21 and set its root to `/tmp`.

## Build
* Recursively clone this repository to local.
* Make sure `boost`, `boost-regex` and `boost-filesystem` development packages are installed.  
On Debian or Ubuntu this can be done with `sudo apt-get install libboost-dev libboost-regex-dev libboost-filesystem-dev`.  
On macOS just run `brew install boost`.
* Build the server with `make`. [Libasync](https://github.com/lqf96/libasync) will be built as dependency at the same time.

## Project Structure
* `headers`
  - `auth.h`: FTP authentication backend. (Not used)
  - `data`
    + `ftp_methods`: All FTP methods supported as a list.
    + `middlewares`: All middlewares applied in request handling process. Executed from top to bottom.
    + `responses`: Response codes and their textual descriptions.
  - `data_conn.h`: FTP data connection classes for active mode and passive mode.
  - `server.h`: FTP server class definition.
  - `session.h`: FTP session class definition.
* `src`
  - `auth.cpp`: FTP authentication backend. (Not used)
  - `data_conn.cpp`: FTP data connection classes for active mode and passive mode.
  - `main.cpp`: Program entry and CLI argument parsing.
  - `methods.cpp`: Implementation of FTP methods.
  - `middlewares.cpp`: Implementation of FTP session middlewares.
  - `server.cpp`: FTP server class.
  - `session.cpp`: FTP session class.

## License
[MIT License](LICENSE)
