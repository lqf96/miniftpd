#pragma once

#include <boost/property_tree/ptree.hpp>

namespace miniftpd
{   //Profile type
    typedef boost::property_tree::ptree Profile;

    //Class declarations
    class FTPServer;
    class AuthBackend;
    class ProfileAuthBackend;
}
