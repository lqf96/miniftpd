#pragma once

#include <netinet/in.h>
#include <memory>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <libasync/socket.h>

using boost::property_tree::ptree;
using libasync::ServerSocket;

namespace miniftpd
{   //Server class
    class Server
    {private:
        //Server data type
        struct ServerData
        {   //Configuration
            ptree config;

            //Constructor
            ServerData(ptree _config) : config(_config) {}
        };

        //Server data reference type
        typedef std::shared_ptr<ServerData> ServerDataRef;

        //Server data
        ServerDataRef data;
        //Server socket
        ServerSocket server_sock;
    public:
        //Constructor
        Server(ptree config);

        //Listen on given address
        void listen(in_addr_t ip = INADDR_NONE, in_port_t port = 0);
        //Close server
        void close();

        //Get server status
        ServerSocket::Status status();
    };
}
