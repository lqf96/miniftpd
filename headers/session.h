#pragma once

#include <string>
#include <memory>
#include <vector>
#include <utility>
#include <unordered_map>
#include <boost/filesystem/path.hpp>
#include <libasync/socket.h>
#include "data_conn.h"

using std::string;
using boost::filesystem::path;

namespace miniftpd
{   //FTP marco
    #define FTP_METHOD(name) string name(string req);
    //FTP middleware
    #define FTP_MIDDLEWARE(name) string name(string req);
    //FTP response
    #define FTP_RESPONSE(code, desc) extern const char* RESP_DESC_##code;

    //FTP response code & description
    #include "data/responses"

    //FTP session class
    class Session
    {private:
        //FTP method handler type
        typedef string (Session::*MethodHandler)(string);

        //Session data type
        struct SessionData
        {   //Current middleware index
            size_t middleware_index;

            //Logged in or not
            enum LoginStatus
            {   NOT_LOGGED,
                USERNAME_GIVEN,
                LOGGED
            } login_status;
            //Username
            string username;

            //Current data connection
            DataConnection* data_conn;
            //Connection available
            bool conn_avail;

            //Root directory
            path root;
            //Current working directory
            path cwd;

            //Constructor
            SessionData(string _root) : login_status(NOT_LOGGED),
                data_conn(nullptr),
                conn_avail(true),
                root(_root),
                cwd("/") {}
            //Destructor
            ~SessionData()
            {   if (data_conn)
                    delete data_conn;
            }
        };

        //Session data reference type
        typedef std::shared_ptr<SessionData> SessionDataRef;

        //Socket
        Socket sock;
        //Session data
        SessionDataRef data;

        //Middlewares
        static std::vector<MethodHandler> middlewares;
        //Method handlers
        static std::unordered_map<string, MethodHandler> method_handlers;

        //Data handler
        void on_data(string req);
        //Close handler
        void on_close();

        //Call next middleware
        string next(string req);

        //FTP methods definition
        #include "data/ftp_methods"
        //Middlewares definition
        #include "data/middlewares"
    public:
        //Constructor
        Session(Socket sock, string root);
    };

    //Parse request
    std::pair<string, string> parse_req(string req);
    //Make response
    string make_resp(unsigned int resp_code, string description);
}
