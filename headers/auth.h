#pragma once

#include <string>
#include <boost/optional.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include "defs.h"

using std::string;
using boost::filesystem::path;
using boost::property_tree::ptree;
using boost::optional;

namespace miniftpd
{   //Authentication backend
    class AuthBackend
    {public:
        //Check if the username can be handled by this backend
        virtual bool can_auth(string username) = 0;
        //Do authentication
        virtual optional<ptree> auth(string username, string password) = 0;
    };

    //Profile authentication backend
    class ProfileAuthBackend : public AuthBackend
    {private:
        //User profile directory path
        path user_profile_dir;
    public:
        //Constructor
        ProfileAuthBackend(FTPServer server);

        //Check if the username can be handled by this backend
        bool can_auth(string username);
        //Do authentication
        optional<ptree> auth(string username, std::string password);
    };
}
