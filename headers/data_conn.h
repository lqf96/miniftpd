#pragma once

#include <netinet/in.h>
#include <string>
#include <iostream>
#include <boost/optional.hpp>
#include <libasync/promise.h>
#include <libasync/socket.h>

using std::string;
using libasync::Socket;
using libasync::ServerSocket;

namespace miniftpd
{   //Data connection type
    class DataConnection
    {public:
        //Send data to client
        virtual libasync::Promise<void> send(string data) = 0;
        //Wait for receiving data
        virtual libasync::Promise<string> recv() = 0;

        //Virtual destructor
        virtual ~DataConnection() {}
    };

    //Active data connection
    class ActiveConnection : public DataConnection
    {private:
        //Control socket
        Socket ctrl_sock;
        //Data socket
        Socket sock;

        //Address
        in_addr_t addr;
        //Port
        in_port_t port;
    public:
        //Constructor
        ActiveConnection(Socket _ctrl_sock, in_addr_t _addr, in_port_t _port);
        //Destructor
        ~ActiveConnection();

        //Send data to client
        libasync::Promise<void> send(string data);
        //Wait for receiving data
        libasync::Promise<string> recv();
    };

    //Passive data connection
    class PassiveConnection : public DataConnection
    {private:
        //Control socket
        Socket ctrl_sock;
        //Server socket
        ServerSocket server_sock;
        //Socket
        boost::optional<Socket> sock;

        //Received data
        string recv_data;
        //Connection closed flag
        bool closed;
    public:
        //Constructor
        PassiveConnection(Socket _ctrl_sock);
        //Destructor
        ~PassiveConnection();

        //Get port
        in_port_t port();
        //Send data to client
        libasync::Promise<void> send(string data);
        //Wait for receiving data
        libasync::Promise<string> recv();
    };
}
